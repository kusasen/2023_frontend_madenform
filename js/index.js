$(function(){
    const slidePeoSwiper = new Swiper('.slide-peo-swiper', {
        slidesPerView: 1,
        loop: false,
        speed: 600,
        autoplay: {
            delay: 8000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
    });

    const recommendSwiper = new Swiper('.recommend-swiper', {
        slidesPerView: 1,
        loop: false,
        speed: 600,
        autoplay: {
            delay: 8000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
    });

    AOS.init();
});