$(function(){
  var imgLoad = imagesLoaded('#wrap');

  var progressBar = $(".progress-count"),
      count = $(".progress-count"),
      images = $("img").length,
      loadedCount = 0,
      loadingProgress = 0,
      tlProgress = new TimelineMax();
  
  imgLoad.on( 'progress', function( instance, image ) {
      loadProgress();
  });
  
  function loadProgress(imgLoad, image) {

      loadedCount++;
    
      loadingProgress = Math.round(loadedCount/images);

      TweenMax.to(tlProgress, 1, {progress:loadingProgress});
  }

  var tlProgress = new TimelineMax({
      paused: true,
      onComplete: loadComplete
  });
  
  tlProgress
      .to(progressBar, 1, {width:"100%"});
  
  function loadComplete() {
    var tlEnd = new TimelineMax();
    tlEnd.to("#loading", 0.5, {opacity:0 ,display: 'none'});

    var indexAnimation = new TimelineMax();
    indexAnimation.fromTo(".ani-b1", .8, {opacity:0 ,x: -100},{opacity:1 ,x: 0 ,ease:Quart.ease})
        .fromTo(".ani-b2", .8, {opacity:0 ,y: -30},{opacity:1 ,y: 0 ,ease:Quart.ease} ,'-=.5')
        .fromTo(".ani-b3", .8, {opacity:0 ,x: -100},{opacity:1 ,x: 0 ,ease:Quart.ease})
        .fromTo(".ani-b4", .8, {opacity:0 ,y: 50},{opacity:1 ,x: 0 ,ease:Quart.ease})
        .fromTo(".ani-b4 .text", .8, {opacity:0 ,scale: 1.2},{opacity:1 ,scale: 1 ,ease:Quart.ease});
  }
})